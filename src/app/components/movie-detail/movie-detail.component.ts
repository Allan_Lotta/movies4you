import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {
  movie: any;
  omdbID: string;
  constructor(private movieService: MovieService,
              private activatedRoute: ActivatedRoute,
              private location: Location) { }

  ngOnInit() {
    this.omdbID = this.activatedRoute.snapshot.paramMap.get("id");
    this.getMovieById(this.omdbID);
  }

  getMovieById(id: string){
    this.movieService.getMoviesById(id).subscribe( res =>{
      this.movie = res;
      })
  }
  
  goBack(){
    this.location.back();
  }
}
