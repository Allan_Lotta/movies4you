import { Component, OnInit, Output } from '@angular/core';

import {Movie} from '../../models/movie';
import { MovieService } from 'src/app/services/movie.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pageCont: number = 1;
  searchValue: string;
  movies: Movie[] = [];
  totalResults: number = 0;
  constructor(private movieService: MovieService) { }

  ngOnInit() {}

  getMovies(searchEvent) { 
    this.pageCont = 1; //restart cont
    this.searchValue = searchEvent;
    if(searchEvent.length > 2){ //for remove err <2 request forEach console
      this.movieService.searchMovie(this.searchValue, this.pageCont).subscribe(res =>{
        this.movies = res['Search']
        this.totalResults = res['totalResults']
      })
    }
  }

  onScroll(){ //search new page on scroll down
    if(this.pageCont < this.totalResults/10){
        this.pageCont ++;
        this.movieService.searchMovie(this.searchValue, this.pageCont).subscribe(res => {
          this.addMovie(res['Search'])
      })
    }
  }
  
  addMovie(data){ //get array of movie on res['Search']
    data.forEach(element => {
      this.movies.push(element);
    });
  }
}
