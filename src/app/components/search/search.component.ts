import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import { MovieService} from '../../services/movie.service';
import { Observable } from 'rxjs';
import {LocalStorageService, SessionStorageService, LocalStorage} from 'ngx-webstorage';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @LocalStorage('name') name
  search = new FormControl('');
  @Output() movies: EventEmitter<any> = new EventEmitter();
  res: Observable<any>;
  constructor(private movieService: MovieService) { }
  
  ngOnInit() {
    if (this.name !== undefined && this.name !== null) {
      this.search.setValue(this.name);
      this.searchMovie(this.name);
    }
  }

  searchMovie(event){
    let nameSearch = ''
    if (event.target) {
      nameSearch = event.target.value;
    } else {
      nameSearch = event
    }
    this.movies.emit(nameSearch);
    this.name = nameSearch;
  }
}
