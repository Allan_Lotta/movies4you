import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  apiKey: string;
  endPoint: string;
  constructor(private http: HttpClient) {
    this.apiKey = '84c4433c&'
    this.endPoint = `http://www.omdbapi.com/?apikey=${this.apiKey}`
   }

  searchMovie(name: string, page: number){
    let st = `${this.endPoint}s=${name}&page=${page}`;
    return this.http.get(`${this.endPoint}s=${name}&page=${page}`);
  }

  getMoviesById(imdbID: string){
    return this.http.get(`${this.endPoint}i=${imdbID}`);
  }

}
